import boto3
import json
import os
import urllib
import tweepy
from parse import get_paren_info, parse

def get_tweets(user):
    tweets = {}
    api = get_api()
    timeline = api.user_timeline(user, tweet_mode="extended")
    for tweet in timeline:
        if not is_retweet(tweet.full_text):
            tweets[tweet.id] = tweet.full_text
    return tweets


def is_retweet(tweet_text):
    return(tweet_text[0:4] == "RT @") 


def create_good_tweet(parentheticals, original_link):
    tweet = '\n'.join(parentheticals)
    tweet += '\n{link}'.format(link=original_link)
    # tweet = tweet.strip() to remove final newline?
    return tweet


def create_error_tweet(original_link):
    tweet = '[Whoops. This tweet had an unbalanced number of parentheses]'
    tweet += '\n{link}'.format(link=original_link)
    # tweet = tweet.strip() to remove final newline?
    return tweet


def get_api():
    auth = tweepy.OAuthHandler(os.environ['consumer_key'], os.environ['consumer_secret'])
    auth.set_access_token(os.environ['access_token'], os.environ['access_token_secret'])
    return tweepy.API(auth)


def send_tweet(tweet):
    api = get_api()
    status = api.update_status(status=tweet)
    return status


def debug_tweet(tweet_text):
    type(tweet_text)
    print(tweet_text)
    tweet_text_bytes = tweet_text.encode('utf-8')
    for index in range(0, 10):
        print(tweet_text_bytes[index])


def handler(event, context):
    
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table("latest_tweet_id")

    latest_tweet_id = table.get_item(
        Key={"latest": "latest"},
        AttributesToGet=["id"]
    )["Item"]["id"]
    
    tweets = get_tweets(os.environ['username'])
    
    for tweet_id in reversed(list(tweets)):
        # make sure all tweets since last latest tweet are processed
        if tweet_id > latest_tweet_id:
            print(f"Tweet id: {tweet_id}")
            tweet_text = tweets[tweet_id]
            paren_info = get_paren_info(tweet_text)
            if paren_info["open_count"] > 0 or paren_info["closed_count"] > 0:
                link = f"https://twitter.com/{os.environ['username']}/status/{tweet_id}"
                if paren_info["open_count"] == paren_info["closed_count"]:
                    list_of_parentheticals = parse(tweet_text)
                    tweet = create_good_tweet(list_of_parentheticals, link)
                else:
                    tweet = create_error_tweet(link)
                print(tweet)
                try:
                    response = send_tweet(tweet)
                    print(response)
                except tweepy.TweepError as e:
                    print(e)
            else:
                print("No parentheses this time")
        if tweet_id > latest_tweet_id:
            table.update_item(
                Key={
                    'latest': 'latest',
                },
                UpdateExpression='SET id = :id',
                ExpressionAttributeValues={
                    ':id': tweet_id
                }
            )