import html

def get_paren_info(statement):
    open_count = statement.count('(')
    closed_count = statement.count(')')
    return {
        "open_count": open_count,
        "closed_count": closed_count
    }


def get_indexes(statement, what_we_want):
    indexes = []
    for index, char in enumerate(statement):
        if char == what_we_want:
            indexes.append(index)
    return indexes


def get_parentheticals(full_statement, opens, closeds):
    parentheticals = []
    for open_paren_index in reversed(opens):
        for closed_paren_index in closeds:
            if closed_paren_index > open_paren_index: # found our partner
                escaped_parenthetical = full_statement[open_paren_index:closed_paren_index + 1]
                parenthetical = html.unescape(escaped_parenthetical) # in case of &amp;, etc.
                parentheticals.insert(0, parenthetical) # keep order
                closeds.remove(closed_paren_index)
                break
    return parentheticals


def parse(body):
    open_indexes = get_indexes(body, "(")
    closed_indexes = get_indexes(body, ")")
    list_of_parentheticals = get_parentheticals(body, open_indexes, closed_indexes)
    return list_of_parentheticals
